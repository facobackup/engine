export default {
    PERF: '0',
    PHYSICS: '1',
    TRANSFORMATION: '2',
    SHADOWS: '3',
    PICK: '4',
    MESH: '5',
    POSTPROCESSING: '6',

    AO: '7',
    CULLING: '8'
}