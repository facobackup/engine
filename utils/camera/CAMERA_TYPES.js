export default {
    FREE: 'free',
    SPHERICAL: 'spherical',

    TOP: 'ortho-top',
    BOTTOM: 'ortho-bottom',
    LEFT: 'ortho-left',
    RIGHT: 'ortho-right',
    FRONT: 'ortho-front',
    BACK: 'ortho-back'
}